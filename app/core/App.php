<?php

class App {
    protected $controller = 'Home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()

    {
        $url = $this->parseURL($_SERVER);

       //Setup Controller and routing
       if (!empty($url) &&  file_exists('../app/controllers/' . $url[0] . '.php')) {
        $this->controller = $url[0]; unset ($url[0]);
    }
       require_once '../app/controllers/' . $this->controller. '.php'; 
       $this->controller = new $this->controller;
       
       //method
       if (isset($url[1])) {
           if (method_exists($this->controller, $url[1])) { 
            $this->method = $url[1]; 
            unset($url[1]);
        }
        
        //params/data and setup the controller
        $this->params = is_array($url) ? array_values($url) : [];
        call_user_func_array(
            [$this->controller, $this->method],
            $this->params
        );
    }
}

public function parseURL(){
        
    if(isset($_GET['url'])) {
        $url = $_GET['url'];return $url;
        return $url;
    }
    
}
}


