<?php

class About extends Controller {
    public function index($nama = 'Diki', $pekerjaan = 'Mahasiswa', $umur = '19')
    {
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['umur'] = $umur;
        $this->view('tamplates/header');
        $this->view('home/index');
        $this->view('tamplates/footer');
    }
    
    public function page()
    {
        $this->view('tamplates/header');
        $this->view('about/page');
        $this->view('tamplates/footer');     
    }
}