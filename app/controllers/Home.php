<?php 

class Home extends Controller {
    public function index()
    {
        $this->view('tamplates/header');
        $this->view('home/index');
        $this->view('tamplates/footer');
    }
}
?>